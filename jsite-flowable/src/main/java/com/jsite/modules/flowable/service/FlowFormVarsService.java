/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.flowable.service;

import com.fasterxml.jackson.databind.JavaType;
import com.jsite.common.mapper.JsonMapper;
import com.jsite.common.persistence.Page;
import com.jsite.common.service.CrudService;
import com.jsite.modules.flowable.dao.FlowFormVarsDao;
import com.jsite.modules.flowable.entity.FlowForm;
import com.jsite.modules.flowable.entity.TaskNode;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 流程表单业务总表Service
 * @author liuruijun
 * @version 2019-04-11
 */
@Service
@Transactional(readOnly = true)
public class FlowFormVarsService extends CrudService<FlowFormVarsDao, TaskNode> {

	@Override
	public TaskNode get(String id) {
		return super.get(id);
	}
	
	@Override
	public List<TaskNode> findList(TaskNode taskNode) {
		return super.findList(taskNode);
	}
	
	@Override
	public Page<TaskNode> findPage(Page<TaskNode> page, TaskNode taskNode) {
		return super.findPage(page, taskNode);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void save(TaskNode taskNode) {
		super.save(taskNode);
	}

    @Transactional(readOnly = false)
    public void saveUpdate(TaskNode taskNode) {
        TaskNode node = dao.get(taskNode);
        if (node == null) {
            taskNode.preInsert();
            dao.insert(taskNode);
        } else {
            taskNode.setId(node.getId());
            taskNode.preUpdate();
            dao.update(taskNode);
        }
    }

	@Transactional(readOnly = false)
	public void insert(TaskNode taskNode) {
		dao.insert(taskNode);
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(TaskNode taskNode) {
		super.delete(taskNode);
	}



	public List<String> varsNodeList(String modelEditorJson) {
		JSONObject jsonObject = new JSONObject (modelEditorJson);
		JavaType javaType = JsonMapper.getInstance().createCollectionType(ArrayList.class, TaskNode.class);
		List<TaskNode> sourceList = JsonMapper.getInstance().fromJson(jsonObject.getJSONArray("childShapes").toString(), javaType);

        String regex = "(\\$|\\{|\\}|!|>|=|<|not|empty|\\d)";
        List<String> varList = new ArrayList<>();
		Iterator<TaskNode> iterator = sourceList.iterator();
		while (iterator.hasNext()) {
			TaskNode node = iterator.next();
			if (node.getStencil().getId().equals("SequenceFlow")) {
				TaskNode.Conditionsequenceflow conditionsequenceflow = node.getProperties().getConditionsequenceflow();
				if (conditionsequenceflow != null && !conditionsequenceflow.getExpression().getStaticValue().equals("")) {
					// && !conditionsequenceflow.getExpression().getStaticValue().contains("auditPass")
                    String var = conditionsequenceflow.getExpression().getStaticValue().replaceAll(regex, "").trim();
                    varList.add(var);
				}
			}
		}

		return varList.stream().distinct().collect(Collectors.toList());
	}

	public TaskNode bindVar(FlowForm flowForm, String fieldName) {
		TaskNode tmp = new TaskNode(fieldName, flowForm.getModelId(), flowForm.getModelKey(), flowForm.getModelVersion());
		return dao.findFormVarByField(tmp);
	}
}